name := "test"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies +=
  "org.typelevel" %% "cats-core" % "1.0.0"

libraryDependencies += 
  "org.scalaz" %% "scalaz-zio" % "0.16"

scalacOptions ++= Seq(
  "-Xfatal-warnings",
  "-Ypartial-unification"
)